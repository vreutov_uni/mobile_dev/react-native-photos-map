import React, { Component } from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import { MapView, FileSystem } from 'expo';
import { Button, Icon } from "native-base";
import PouchDB from 'pouchdb-react-native'

import { DB_NAME } from 'react-native-dotenv'
import { common_styles } from './theme/styles'

function randomColor() {
    return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

let id = 0;

export default class Map extends Component {
    constructor(props) {
        super(props);

        this.db = new PouchDB(DB_NAME,
            {adapter: 'react-native-sqlite'});

        var self = this;

        this.db.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', function(change) {
            console.log(change);
            
            if (change.deleted) {
                self.onDeleteMarker(change.doc);
            } else {
                self.onAddMarker(change.doc);
            }
        }).on('error', function(err) {
            console.log(err);
        })

        this.state = {
            region: {
                latitude: 58.014604,
                longitude: 56.247246,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            markers: []
        };

        this.loadMarkers();
    }

    clearAllMarkers() {
        this.db.allDocs({include_docs: true})
        .then(function (result) {
            markers = result.rows.forEach(row => {
                this.removeMarker(row.doc);
            });
        }.bind(this));
    }

    removeMarker(marker) {
        this.beforeMarkerDelete(marker);
        return this.db.remove(marker);
    }

    loadMarkers() {
        this.db.allDocs({include_docs: true})
        .then(function (result) {
            markers = result.rows.map(function (row) {
                return row.doc;
            });

            this.setState({
                markers: markers
            })
        }.bind(this));
    }

    onAddMarker(marker) {
        var index = this.state.markers.findIndex(item => {
            return item._id === marker._id;
        });

        if (index !== -1)
            return;

        this.setState({
            markers: [
                ...this.state.markers,
                marker
            ]
        })
                
        if (this.state.lastMarker !== undefined) 
        {
            this.removeMarkerIfEmpty(this.state.lastMarker._id);
        }

        this.setState({
            lastMarker: marker
        })
    }

    beforeMarkerDelete(marker) {
        marker.photos.forEach(photo => {
            FileSystem.deleteAsync(photo, {idempotent: true});
        });
    }

    onDeleteMarker(marker) {
        prevMarkers = this.state.markers.slice();
        var index = prevMarkers.findIndex(function (item) {
            return item._id === marker._id;
        });

        if (index !== -1) {
            prevMarkers.splice(index, 1);

            this.setState({
                markers: prevMarkers,
            });
        }
    }

    removeMarkerIfEmpty(id) {
        return this.db.get(id).then((marker) => {
            if (marker.photos.length === 0) {
                return this.removeMarker(marker);
            }
        })
    }

    onMapPress(e) {
        var marker = {
            "coordinate": e.nativeEvent.coordinate,
            "color": randomColor(),
            "photos": []
        };

        this.db.post(marker);
    }
    
    onCalloutPress(marker) {
        if (this.props.navigation !== undefined) {
            this.props.navigation.navigate('Images', {
                markerId: marker._id
            });
        }
    }

    onTrashPress() {
        Alert.alert(
            'Confirmation',
            'Delete all markers?',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => this.clearAllMarkers()},
            ],
            { cancelable: false }
        );
    }

    debugAlert(message) {
        console.log(message);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={{ flex: 1, alignSelf: 'stretch' }}
                    initialRegion={this.state.region}
                    onPress={(e) => this.onMapPress(e)}
                >
                    {this.state.markers.map(marker => (
                        <MapView.Marker
                            key={marker._id}
                            coordinate={marker.coordinate}
                            pinColor={marker.color}
                        >
                            <MapView.Callout style={styles.plainView}
                                onPress={() => this.onCalloutPress(marker)}
                            >
                                <View>
                                    <Text>Add new photo...</Text>
                                </View>
                            </MapView.Callout>
                        </MapView.Marker>
                    ))}
                </MapView>

                <Button style={common_styles.float_button}
                    onPress={() => this.onTrashPress()}
                >
                    <Icon name='trash' />
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    plainView: {
      width: 105,
    }
});