import {
    StyleSheet,
    Platform
} from 'react-native';

export const header_style = StyleSheet.create({
    left: {
        ...Platform.select({
            android: {
                flex: 0, paddingLeft: 6, width: 62
            },
        })
    },
    body: {
        ...Platform.select({
            android: {
                flex: 1
            },
        })
    },
    right: {
        ...Platform.select({
            android: {
                flex: 0
            },
        })
    }
});

export const common_styles = StyleSheet.create({
    float_button: {
        width: 60,
        height: 60,
        borderRadius: 30,     
        backgroundColor: '#ee6e73',                                    
        position: 'absolute',                                          
        bottom: 10,                                                    
        right: 10,
        justifyContent: 'center'
    }
});