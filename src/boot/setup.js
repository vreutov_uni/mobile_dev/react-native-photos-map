import * as Expo from "expo";
import React, { Component } from "react";
import { StyleProvider } from "native-base";

import PouchDB from 'pouchdb-react-native'
import SQLiteAdapterFactory from 'pouchdb-adapter-react-native-sqlite'

import App from "../App";

export default class Setup extends Component {
	constructor() {
		super();
		this.state = {
			fontsReady: false,
			dbReady: false
		};
	}
	componentWillMount() {
		this.loadFonts();
		this.setupDB();
	}

	async loadFonts() {
		await Expo.Font.loadAsync({
			Roboto: require("native-base/Fonts/Roboto.ttf"),
			Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
			Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
		});

		this.setState({ fontsReady: true });
	}

	async setupDB() {
		const SQLiteAdapter = SQLiteAdapterFactory(Expo.SQLite);
		PouchDB.plugin(SQLiteAdapter);

		this.setState({ dbReady: true });
	}

	isReady() {
		return this.state.fontsReady && this.state.dbReady;
	}

	render() {
		if (!this.isReady()) {
			return <Expo.AppLoading />;
		}
		return (
			<App />
		);
	}
}