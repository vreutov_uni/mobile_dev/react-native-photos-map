import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import { StackNavigator } from 'react-navigation';
import { MapView, Constants} from 'expo';

import { header_style } from "./theme/styles";

import Map from "./Map";
import ImagesScreen, { FullImageScreen } from "./Images"

class HomeScreen extends Component {
	static navigationOptions = {
		title: 'React Photo Map'
	};

	render() {
		return (
			<Map navigation={this.props.navigation}/>
		);
	}
}

const RootStack = StackNavigator(
	{
		Home: {
			screen: HomeScreen
		},
		Images: {
			screen: ImagesScreen
		},
		FullImage: {
			screen: FullImageScreen
		}
	},
	{
		initialRouteName: 'Home',

		navigationOptions: {
			headerStyle: {
				backgroundColor: '#3F51B5',
			},
			headerTintColor: '#FFF',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
		},
	}
);

export default class App extends React.Component {
	render() {
		return <RootStack />;
	}
}
