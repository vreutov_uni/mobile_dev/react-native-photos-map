import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Container, Content, Card, CardItem, Button, Icon, Left, Body, Right, Text } from 'native-base';
import PouchDB from 'pouchdb-react-native'
import { ImagePicker } from 'expo';

import { DB_NAME } from 'react-native-dotenv'
import { common_styles } from './theme/styles'

export default class ImagesScreen extends Component {
    static navigationOptions = {
        title: 'Photos'
    };

    constructor(props) {
        super(props);

        const { params } = this.props.navigation.state;
        
        this.db = new PouchDB(
            DB_NAME,
            {adapter: 'react-native-sqlite'});
        
        this.state = {
            marker: null
        };

        this.db.get(params.markerId).then(result => {
            this.setState({
                marker: result
            });
        });
    }

    onAddPress = async() => {
        let result = await ImagePicker.launchCameraAsync({
            quality : 0.7,
        });

        if (!result.cancelled) {
            var id = this.state.marker._id;

            this.db.get(id).then(marker => {
                marker.photos.push(result.uri);
                return this.db.put(marker);
            }).then(() => {
                return this.db.get(id);
            }).then(marker => {
                this.setState({
                    marker: marker
                });
            });
        }
    }

    onDeletePress(photoUri) {
        console.log(photoUri);
    }

    onImagePress(photoUri) {
        this.props.navigation.navigate('FullImage', {
            uri: photoUri
        });
    }

    render() {
        if (this.state.marker === null) {
            return <Container/>;
        }

        return (
            <Container>
                <Content>
                    <ScrollView>
                        {this.state.marker.photos.length === 0 &&
                            <Card>
                                <CardItem>
                                    <Body>
                                        <Text>There's no photos yet. Go ahead and add some!</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        }
                        {this.state.marker.photos.map(photo => (
                            <Card
                                key={photo}
                            >
                                <CardItem cardBody>
                                    <TouchableOpacity activeOpacity={0.9}
                                        onPress={() => this.onImagePress(photo)} style={{ flex: 1 }}
                                    >
                                        <Image source={{uri: photo}} style={{height: 200, width: null, flex: 1}} />
                                    </TouchableOpacity>
                                </CardItem>
                                <CardItem>
                                    <Body>
                                    </Body>
                                    <Right>
                                        <Button iconLeft transparent danger
                                            onPress={() => this.onDeletePress(photo)}
                                        >
                                            <Icon name="trash" />
                                            <Text>Delete</Text>
                                        </Button>
                                    </Right>
                                </CardItem>
                            </Card>
                        ))}
                    </ScrollView>
                </Content>

                <Button style={common_styles.float_button}
                    onPress={() => this.onAddPress()}
                >
                    <Icon name='add' />
                </Button>
            </Container>
        );
    }
}

export class FullImageScreen extends Component {
    static navigationOptions = {
        title: 'Photo View'
    };

    constructor(props) {
        super(props);

        const { params } = this.props.navigation.state;

        this.state = { 
            uri: params.uri
        };
    }
    render() {
        return (
            <View style={{flex: 1, alignItems: 'stretch'}}>
                <Image style={{ flex: 1 }} source={{uri: this.state.uri}} />
            </View>
        );
    }
}
